# bake

bake is Copper's package manager. It is a source based package manager
written in C3.

# Usage
```
usage: bake [args]

    s        - Syncs the repos
    u        - Updates packages
    i [name] - Installs a package
    r [name] - Removes a package
```
